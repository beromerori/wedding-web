import { CONST } from '@/utils/constants';

const hotels = [
    {
        type: CONST.ESTABLISHMENT.HOTEL,
        name: 'Hotel Zeus',
        address: 'Av. Reina Sofia, 8',
        phone: '924315564',
        url: 'http://www.hotelzeusmerida.com',
        hasModal: true,
        info: 'Es importante que sepáis, que <span class="font-bold">NO NOS HA BLOQUEADO HABITACIONES.'
    },
    {
        type: CONST.ESTABLISHMENT.HOTEL,
        name: 'Hotel Velada',
        address: 'Av. Reina Sofia, s/n',
        phone: '924315110',
        url: 'https://www.hotelveladamerida.com/?utm_source=google&utm_medium=business&utm_campaign=maps',
        hasModal: true,
        info: `
            <div class="space-y-4">
                <p>Aquí la reserva se hace a través de correo electrónico  a <span class="font-bold">reservas.merida@veladahoteles.com<span></p>
                <p>Escribir correo electrónico y poner como referencia 
                    <span class="font-bold">"BODA NO HOTEL URBANO Y ALICIA"</span> 
                    especificando noches de la estancia y facilitando nombre completo, teléfono de contacto y un número de tarjeta de crédito que garantice las condiciones de pago y cancelación pactadas.
                </p>
                <p>Nos bloquean habitaciones hasta el <span class="font-bold">24/04/23</span>.</p>
                <p>Ninguna reserva se dará por realizada hasta tener confirmación escrita por parte del hotel con su correspondiente número de localizador.</p>
                
                <ul class="space-y-2">
                    <li>
                        <p class="font-bold">Habitación Doble:</p> 
                        <p>1 noche 70€ / 2 noches 130€</p>
                    </li>
                    <li>
                        <p class="font-bold">Habitación Individual:</p>
                        <p>1 noche 65€ / 2 noches 120€</p>
                    </li>
                    <li>
                        <p class="font-bold">Habitación Triple Niño (Hasta 12 años):</p> 
                        <p>1 noche 80€ / 2 noches 150€</p>
                    </li>
                    <li>
                        <p class="font-bold">Habitación Triple:</p> 
                        <p>1 noche 96€ / 2 noches 180€</p>
                    </li>
                    <li>
                        <p class="font-bold">Desayuno Buffet:</p> 
                        <p>10,80 euros/persona (opcional)</p>
                    </li>
                </ul>
                <div class="space-y-2">
                    <p class="font-bold">Cancelaciones:</p>
                    <ul class="space-y-2">
                        <li>- Cancelación sin gastos hasta 45 días antes.</li>
                        <li>- Cancelación con menos de 45 días gastos del 30% del total cancelado.</li>
                        <li>- Cancelación con menos de 35 días gastos del 40% del total cancelado.</li>
                        <li>- Cancelación con menos de 21 días gastos del 50% del total cancelado.</li>
                        <li>- Cancelación con menos de 14 días gastos del 100% del total cancelado.</li>
                    </ul>
                </div>
                <p>
                    A partir de 21 días antes de la llegada el Hotel realizará un cargo, por el importe total de la estancia, en la tarjeta facilitada a tal efecto. 
                    En caso de incumplir los pagos el Hotel se reserva el derecho a cancelar.
                </p>
            </div>
        `
    },
    {
        type: CONST.ESTABLISHMENT.HOTEL,
        name: 'Hostal Anas',
        address: 'Av. Reina Sofia, 9',
        phone: '924311113',
        url: 'http://www.hostalanas.com/',
        hasModal: true,
        info: `
            <div class="space-y-4">
                <p>Es importante que sepáis, que 
                    <span class="font-bold">NO NOS HA BLOQUEADO HABITACIONES.<span>
                </p>
                <p>
                    La manera de reservar es llamar y decir que es para la 
                    <span class="font-bold">boda de Alicia y Urbano</span>.
                </p>
                <p>El precio por noche  es:</p>
                <ul class="space-y-2">
                    <li>
                        <span class="font-bold">Habitación doble: </span>50€ / noche
                    </li>
                    <li>
                        <span class="font-bold">Habitación triple: </span>70€ / noche
                    </li>
                    <li>
                        <span class="font-bold">Habitación individual: </span>35€ / noche
                    </li>
                </ul>
                <p>
                    Podéis cancelar hasta 48 horas antes, al llamar tendréis que facilitar un número de tarjeta, para bloquear la habitación, no se os hará cargo hasta el día de llegada.
                </p>
            </div>
        `
    },
    {
        type: CONST.ESTABLISHMENT.HOTEL,
        name: 'Hotel Romero',
        address: 'AUTOVIA A - 66, Km 630',
        description: 'Es el que está más próximo al lugar de la celebración',
        phone: '671682458',
        url: 'http://hotelromero.com',
        hasModal: true,
        info: `
            <div class="space-y-4">
                <p>
                    La manera de reservar es llamar y decir que es para la 
                    <span class="font-bold">boda de Alicia y Urbano</span>.
                </p>
                <p>
                    <span class="font-bold">Habitación doble: </span>60€ / noche
                </p>
                <div>
                    <p>- Acceso a la piscina incluida.</p> 
                    <p>- Posibilidad de salir una hora más tarde de la habitación siendo esta a las 13:00h.</p>
                </div>
            </div>
        `
    },
    {
        type: CONST.ESTABLISHMENT.HOTEL,
        name: 'Hotel Ilunion Las Lomas',
        address: 'Av. Reina Sofia, 78',
        phone: '924311011',
        url: 'https://www.ilunionlaslomas.com',
        hasModal: true,
        info: `
            <div class="space-y-4">
                <p>Este hotel nos ha bloqueado 30 habitaciones.</p>
                <ul class="space-y-2">
                    <li>
                        <span class="font-bold">Habitación doble: </span>70€ / noche
                    </li>
                    <li>
                        <span class="font-bold">Suplemento cama supletoria: </span>21€
                    </li>
                </ul>
                <p>Máximo 3 personas por habitación, incluido niños.</p>
                <p>
                    En este caso nos tenéis que escribir a nosotros, y haremos un listado que un mes antes enviaremos al hotel.
                </p>
            </div>
        `
    },
];

const hairdressingMakeup = [
    {
        type: CONST.ESTABLISHMENT.HAIRDRESSING_MAKEUP,
        name: 'Peluquería Crisanto Manuel Barrena',
        address: 'C/ Vespasiano, 6',
        phone: '924302872'
    },
    {
        type: CONST.ESTABLISHMENT.HAIRDRESSING_MAKEUP,
        name: 'Peluquería Cuca Bautista',
        address: 'C/ Berzocana, 4',
        phone: '924330468'
    },
    {
        type: CONST.ESTABLISHMENT.HAIRDRESSING_MAKEUP,
        name: 'Ondas peluquería y belleza',
        address: 'C/ Baños, 31',
        phone: '924301010'
    },
    {
        type: CONST.ESTABLISHMENT.HAIRDRESSING_MAKEUP,
        name: 'Cristian Reina Peluquero',
        address: 'C/ Jose Ramón Mélida, 8',
        phone: '655211263'
    },
    {
        type: CONST.ESTABLISHMENT.HAIRDRESSING_MAKEUP,
        name: 'Centro de estética Carmen Peña',
        address: 'C/ Vespasiano, 5',
        phone: '924303800'
    },
    {
        type: CONST.ESTABLISHMENT.HAIRDRESSING_MAKEUP,
        name: 'Clínica Bioestetika',
        address: 'Plaza de los escritores, 6',
        phone: '603628604'
    },
];

const restaurants = [
    {
        type: CONST.ESTABLISHMENT.RESTAURANT,
        name: 'Vaova Gastro',
        address: 'C/ Diego Muñoz Torrero, 2',
        phone: '924963181'
    },
    {
        type: CONST.ESTABLISHMENT.RESTAURANT,
        name: 'Agallas Gastro & Food',
        address: 'C/ Suares Somente, 2',
        phone: '679700670'
    },
    {
        type: CONST.ESTABLISHMENT.RESTAURANT,
        name: 'Mahalo',
        address: 'Plaza de los escritores S/n',
        phone: '623369750'
    },
    {
        type: CONST.ESTABLISHMENT.RESTAURANT,
        name: 'Mesón los bodegones',
        address: 'Plaza de los escritores S/n',
        phone: '924047536'
    },
    {
        type: CONST.ESTABLISHMENT.RESTAURANT,
        name: 'Las siete sillas',
        description: 'Pizzas italianas',
        address: 'C/ Jose Ramón Melida, 49',
        phone: '722175126'
    },
    {
        type: CONST.ESTABLISHMENT.RESTAURANT,
        name: 'Rainha do Mar',
        address: 'Avda. Papa Juan XXIII',
        phone: '924522733'
    },
    {
        type: CONST.ESTABLISHMENT.RESTAURANT,
        name: 'Churrería zona sur',
        description: 'Churros para llevar.',
        address: 'C/ Vicente Aleixandre. (Frente al BBVA)'
    },
    {
        type: CONST.ESTABLISHMENT.RESTAURANT,
        name: 'La esquina de Don Mollete',
        address: 'Camino del peral, 9',
        phone: '924522588'
    },
];

export class InterestPointsService {
    getInterestPoints() {
        return {
            hotels,
            restaurants,
            hairdressingMakeup
        }
    }
};