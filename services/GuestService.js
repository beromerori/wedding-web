import axios from 'axios';

export const GuestService = {

    async getGuestList() {

        try {
            const { data } = await axios.get('https://wedding-web-prod.vercel.app/api/guests');
            return data;
        }
        catch (error) {
            console.error('Error getting guest list', error);
            return [];
        }
    },

    async addGuest(guest) {

        try {
            const { data } = await axios.post('/guests', guest);
            return data;
        }
        catch (error) {
            console.error('Error adding guest', error);
            return null;
        }
    }
};

export default {
    GuestService
}