import { InterestPointsService } from "~/services/InterestPointsService";
import { GuestService } from "~/services/GuestService";

export default ({ app, $axios }, inject) => {

    const services = {
        interestPoints: new InterestPointsService(),
        guests: GuestService
    };

    inject('services', services);
}